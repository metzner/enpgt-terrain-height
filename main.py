import ast
import math
import os.path
import re
import shutil
import sys
from collections import defaultdict
from datetime import datetime
from functools import lru_cache
from typing import DefaultDict, Dict, List

import numpy as np
import pyproj as pyproj
import shapely as shapely
from tqdm import tqdm


BUFFER_RADIUS = 500
PIXEL_WIDTH = 200


def weighted_avg_and_std(values: List[float], weights: List[float]):
    """
    Return the weighted average and standard deviation.

    values, weights -- NumPy ndarrays with the same shape.
    """
    average = np.average(values, weights=weights)
    # Fast and numerically precise:
    variance = np.average((np.array(values)-average)**2, weights=weights)
    return average, math.sqrt(variance)


@lru_cache
def get_terrain_heights():
    terrain_heights: DefaultDict[int, Dict[int, float]] = defaultdict(lambda: dict())
    num_lines = sum(1 for _ in open("dgm200_utm32s.xyz"))
    with open("dgm200_utm32s.xyz") as terrain_file:
        for i, line in enumerate(tqdm(terrain_file, total=num_lines, desc="Parsing terrain height file.")):
            line = line.strip()
            if not line:
                continue

            line_split = line.split(" ")
            terrain_heights[int(line_split[0])][int(line_split[1])] = float(line_split[2])

    return terrain_heights


def do_single_terrain_height(lat: float, lng: float, desired_radius: int):
    terrain_heights = get_terrain_heights()

    transproj = pyproj.transformer.Transformer.from_crs("EPSG:4326", "EPSG:25832", always_xy=True)
    x, y = transproj.transform(lng, lat)

    buffer_poly: shapely.Polygon = shapely.Point(x, y).buffer(desired_radius + BUFFER_RADIUS, resolution=40)
    radius_poly: shapely.Polygon = shapely.Point(x, y).buffer(desired_radius, resolution=40)

    min_x, min_y, max_x, max_y = buffer_poly.bounds
    q_min_x, q_min_y = PIXEL_WIDTH * round(min_x/PIXEL_WIDTH), PIXEL_WIDTH * round(min_y / PIXEL_WIDTH)
    q_max_x, q_max_y = PIXEL_WIDTH * round(max_x/PIXEL_WIDTH), PIXEL_WIDTH * round(max_y / PIXEL_WIDTH)

    pixel_areas = dict()

    for pixel_x in range(q_min_x, q_max_x + 1, PIXEL_WIDTH):
        for pixel_y in range(q_min_y, q_max_y + 1, PIXEL_WIDTH):
            left_top = (pixel_x - PIXEL_WIDTH/2, pixel_y - PIXEL_WIDTH/2)
            left_bottom = (pixel_x - PIXEL_WIDTH/2, pixel_y + PIXEL_WIDTH/2)
            right_bottom = (pixel_x + PIXEL_WIDTH/2, pixel_y + PIXEL_WIDTH/2)
            right_top = (pixel_x + PIXEL_WIDTH/2, pixel_y - PIXEL_WIDTH/2)

            pixel_poly = shapely.Polygon([left_top, left_bottom, right_bottom, right_top]).intersection(radius_poly)
            if shapely.is_empty(pixel_poly):
                continue

            pixel_areas[(pixel_x, pixel_y)] = shapely.area(pixel_poly)

    mean, stddev = weighted_avg_and_std([terrain_heights[x][y] for x, y in pixel_areas], list(pixel_areas.values()))

    mean = round(mean, 5)
    stddev = round(stddev, 5)

    if len(pixel_areas) < 2:
        stddev = "None"

    if not len(pixel_areas):
        mean = "None"

    return mean, stddev, len(pixel_areas)


def do_terrain_height(infile: str, output_dir: str, radius: int):
    print("---")
    print(f"Getting terrain height stats for file {infile} with radius {radius}m.")

    shutil.copyfile(infile, os.path.join(output_dir, os.path.basename(infile)))

    outfile = os.path.join(output_dir, f"output_{radius}.csv")

    num_lines = sum(1 for _ in open(infile))

    with open(infile) as task_file:
        with open(outfile, "w") as output_file:
            output_file.write("id,lat,lng,amount_of_pixels,terrain_height_mean,terrain_height_stddev\n")

            for i, line in enumerate(tqdm(task_file, total=num_lines, desc="Doing Terrain Height.")):
                line = line.strip()
                line_split = re.split('[;,\\t]', line)

                avg = "None"
                stddev = "None"

                try:
                    avg, stddev, pixelamt = do_single_terrain_height(float(line_split[1]), float(line_split[2]), radius)
                except Exception:
                    print(f"Could not interpet line: {line}")

                output_file.write(f"{line_split[0]},{line_split[1]},{line_split[2]},{pixelamt},{avg},{stddev}\n")


def output_readme(file_path: str, radii: List[int]):
    with open(file_path, "w") as f:
        f.write("TERRAIN HEIGHT TOOL\n\n"
                "This file contains some information on how the data was generated and how to interpret it.")

        f.write("\n\n-----\n\n"
                "TERRAIN HEIGHT DATASET\n\n"
                f"This tool uses the DGM{PIXEL_WIDTH} Terrain Height dataset.\n"
                f"This dataset yields the elevation of x-y-coordinate pairs"
                f" on a grid with a resolution of {PIXEL_WIDTH}m.\n"
                "You can find the dataset here:\n"
                "https://gdz.bkg.bund.de/index.php/default/digitale-geodaten/digitale-gelandemodelle/digitales-gelandemodell-gitterweite-200-m-dgm200.html.")

        f.write("\n\n-----\n\n"
                "INPUT AND OUTPUT\n\n"
                "Each line in the task file defines a location.\n"
                "The Terrain Height Tool will extract and average out the elevation values"
                " in a radius around each location.\n\n")

        radius_text = (f"In this case, the radius {radii[0]}m was used."
                       f" The output can be viewed in \"output_{radii[0]}.csv\".")
        if len(radii) > 1:
            radius_text = (f"In this case, the radii {', '.join((str(radius) for radius in radii[:-1]))}"
                           f" and {radii[-1]} were used. For each radius, you'll be able to find an output file, for"
                           f" example: \"output_{radii[0]}.csv\".")

        f.write(radius_text + "\n")

        f.write("An output file usually contains the id, lat and lng of each location, followed by the Terrain"
                " Height values calculated by the tool.")

        f.write("\n\n-----\n\n"
                "WHAT EXACTLY WAS DONE FOR THIS TASK?\n\n")

        f.write("After collecting all the grid points in a radius around a location, an arithmetic mean and standard"
                " deviation are calculated for the terrain height based"
                f" on {PIXEL_WIDTH}m*{PIXEL_WIDTH}m squares around each grid point.\n"
                "If a square only partially falls within the radius"
                ", it is only partially considered. (weighted mean & stddev by area within the radius)\n")

        f.write("\n-----\n\n"
                "OUTPUT\n\n")

        f.write("Each output has the following parameters:\n\n"
                "id - Unique identifier for location.\n"
                "lat - latitude in WGS84.\n"
                "lng - longitude in WGS84.\n"
                "amount_of_pixels - Amount of squares that fell within the area.\n"
                "terrain_height_mean - Weighted-by-area mean of terrain height values that fall within the radius.\n"
                "terrain_height_stddev - Weighted-by-area standard deviation of terrain height values that fall within the radius.\n")


def main(args):
    assert os.path.isfile("dgm200_utm32s.xyz"), "Dataset not found. Please download the xyz version from https://gdz.bkg.bund.de/index.php/default/digitale-geodaten/digitale-gelandemodelle/digitales-gelandemodell-gitterweite-200-m-dgm200.html"

    default_task_file = "task.txt"

    radii = [100, 200, 500, 1000, 2000]

    for arg in args:
        if arg.startswith("[") and arg.endswith("]"):
            radii = ast.literal_eval(arg)

        elif arg.isnumeric():
            radii = [int(arg)]

        elif os.path.isfile(arg):
            default_task_file = arg

        else:
            print(f"Could not find file {arg}.")
            return -1

    if not os.path.isfile(default_task_file):
        print(f"No task file was specified, and {default_task_file} was not found. Don't know what to work on.")
        # print("For now, you need a taskfile in task.txt with each row being ID,lat,lng in WGS84 format.")
        # print("The ID has to just be some unique identifier.")
        return -1

    output_dir = os.path.join("outputs", datetime.now().strftime("%d-%m-%Y_%H-%M-%S"))
    os.makedirs(output_dir)

    for radius in radii:
        do_terrain_height(default_task_file, output_dir, radius)

    output_readme(os.path.join(output_dir, "readme.txt"), radii)


if __name__ == "__main__":
    main(sys.argv[1:])
